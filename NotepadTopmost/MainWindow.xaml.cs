﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace NotepadTopmost
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {        
        static string xmlFilePath = "";
        public MainWindow()
        {
            InitializeComponent();

            // Read a file
            if (!File.Exists(xmlFilePath))
                return;

            // Deserialize the XML back to the object
            XmlSerializer serializer = new XmlSerializer(typeof(MyClass));
            using (TextReader reader = new StreamReader(xmlFilePath))
            {
                MyClass obj = (MyClass)serializer.Deserialize(reader);
                txtboxMain.Text=obj.Data.ToString();

                ComboBoxItem comboBoxItem = new ComboBoxItem();
                //comboBox.SelectedValue = obj.Font;
            }            
        }
        static MainWindow()
        {            
            xmlFilePath = System.IO.Path.GetTempPath() + "NotepadRecovaryData.xml";
        }        

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem = (ComboBoxItem)comboBox.SelectedValue;
            
            if (comboBox.SelectedItem == null)
            {
                int defaultValue = 2;
                comboBox.SelectedItem = defaultValue;
            }

            MyClass obj = new MyClass
            {
                Data = txtboxMain.Text,
                Font = Double.Parse(comboBoxItem.Content.ToString())
            };

            // Serialize the object to XML
            XmlSerializer serializer = new XmlSerializer(typeof(MyClass));
            using (TextWriter writer = new StreamWriter(xmlFilePath))
            {
                serializer.Serialize(writer, obj);
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem comboBoxItem = new ComboBoxItem();
            comboBoxItem = (ComboBoxItem)comboBox.SelectedValue;
            txtboxMain.FontSize= Double.Parse(comboBoxItem.Content.ToString());
        }
    }
    public class MyClass
    {
        public string Data { get; set; }
        public double Font { get; set; }        
    }
}
